﻿using ado.net_homework12.DataContext;
using ado.net_homework12.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework12.Services
{
    public class CityServices
    {
        public List<City> GetCitiesList()
        {
            using (var context= new CityContext())
            {
                var cities = context.Cities.ToList();
                return cities;
            }      
        }

        public bool IfNumberExist(City city, string number)
        {
            using (var context = new CityContext())
            {
                var ifExistsNumber = context.PhoneNumbers.Where(x => x.CitiesId == city.Id).Any(x => x.Number == number);
                return ifExistsNumber;
            }
        }

        public City GetCityByName(string cityName)
        {
            using (var context = new CityContext())
            {
                var city = context.Cities.Where(x=>x.CityName==cityName).ToList().FirstOrDefault();
                return city;
            }
        }

        public void AddNumber(string number)
        {
            using (var context = new CityContext())
            {
              
            }
        }

        public List<PhoneNumbers> GetNumbersByCity(string Name)
        {
            using (var context = new CityContext())
            {
                var cityName = context.Cities.Where(x => x.CityName == Name).ToList().FirstOrDefault();
                var phoneNumbers = context.PhoneNumbers.Where(x=>x.CitiesId==cityName.Id).ToList();
                return phoneNumbers;
            }
        }
    }
}
