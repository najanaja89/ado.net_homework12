﻿namespace ado.net_homework12
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CitiesList = new System.Windows.Forms.ListBox();
            this._ado_net_homework12_DataContext_CityContextDataSet = new ado.net_homework12._ado_net_homework12_DataContext_CityContextDataSet();
            this.phoneNumbersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.phoneNumbersTableAdapter = new ado.net_homework12._ado_net_homework12_DataContext_CityContextDataSetTableAdapters.PhoneNumbersTableAdapter();
            this.NumbersList = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._ado_net_homework12_DataContext_CityContextDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phoneNumbersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // CitiesList
            // 
            this.CitiesList.FormattingEnabled = true;
            this.CitiesList.Location = new System.Drawing.Point(25, 34);
            this.CitiesList.Name = "CitiesList";
            this.CitiesList.Size = new System.Drawing.Size(120, 316);
            this.CitiesList.TabIndex = 0;
            this.CitiesList.Click += new System.EventHandler(this.CitiesList_Click);
            this.CitiesList.SelectedIndexChanged += new System.EventHandler(this.CitiesList_SelectedIndexChanged);
            this.CitiesList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.CitiesList_MouseDoubleClick);
            // 
            // _ado_net_homework12_DataContext_CityContextDataSet
            // 
            this._ado_net_homework12_DataContext_CityContextDataSet.DataSetName = "_ado_net_homework12_DataContext_CityContextDataSet";
            this._ado_net_homework12_DataContext_CityContextDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // phoneNumbersBindingSource
            // 
            this.phoneNumbersBindingSource.DataMember = "PhoneNumbers";
            this.phoneNumbersBindingSource.DataSource = this._ado_net_homework12_DataContext_CityContextDataSet;
            // 
            // phoneNumbersTableAdapter
            // 
            this.phoneNumbersTableAdapter.ClearBeforeFill = true;
            // 
            // NumbersList
            // 
            this.NumbersList.FormattingEnabled = true;
            this.NumbersList.Location = new System.Drawing.Point(227, 34);
            this.NumbersList.Name = "NumbersList";
            this.NumbersList.Size = new System.Drawing.Size(120, 316);
            this.NumbersList.TabIndex = 1;
            this.NumbersList.SelectedIndexChanged += new System.EventHandler(this.NumbersList_SelectedIndexChanged);
            this.NumbersList.DoubleClick += new System.EventHandler(this.NumbersList_DoubleClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(245, 356);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "add number";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.NumbersList);
            this.Controls.Add(this.CitiesList);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._ado_net_homework12_DataContext_CityContextDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phoneNumbersBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox CitiesList;
        private _ado_net_homework12_DataContext_CityContextDataSet _ado_net_homework12_DataContext_CityContextDataSet;
        private System.Windows.Forms.BindingSource phoneNumbersBindingSource;
        private _ado_net_homework12_DataContext_CityContextDataSetTableAdapters.PhoneNumbersTableAdapter phoneNumbersTableAdapter;
        private System.Windows.Forms.ListBox NumbersList;
        private System.Windows.Forms.Button button1;
    }
}

