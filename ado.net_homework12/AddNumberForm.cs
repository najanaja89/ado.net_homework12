﻿using ado.net_homework12.DataContext;
using ado.net_homework12.Models;
using ado.net_homework12.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ado.net_homework12
{
    public partial class AddNumberForm : Form
    {
        public AddNumberForm(string cityName)
        {
            InitializeComponent();
            textBox2.Text = cityName;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CityServices cityServices = new CityServices();
            var city=cityServices.GetCityByName(textBox2.Text);

            if (cityServices.IfNumberExist(city, textBox1.Text))
            {
                MessageBox.Show("number exists");
            }
            else
            {
                PhoneNumbers phoneNumbers = new PhoneNumbers
                {
                    CitiesId = city.Id,
                    Number= textBox1.Text
                };

                using (var context = new CityContext())
                {
                    context.PhoneNumbers.Add(phoneNumbers);
                    context.SaveChanges();
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
