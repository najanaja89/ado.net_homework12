﻿using ado.net_homework12.DataContext;
using ado.net_homework12.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ado.net_homework12
{
    public partial class MainForm : Form
    { 
        public MainForm()
        {
            InitializeComponent();
            CityServices cityServices = new CityServices();
            
            foreach (var city in cityServices.GetCitiesList())
            {
                CitiesList.Items.Add(city.CityName);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_ado_net_homework12_DataContext_CityContextDataSet.PhoneNumbers' table. You can move, or remove it, as needed.
           
        }

        private void CitiesList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CitiesList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            NumbersList.Items.Clear();
            CityServices cityServices = new CityServices();
            var city = cityServices.GetCitiesList().Where(x => x.CityName == CitiesList.Text).FirstOrDefault();
            foreach (var phone in cityServices.GetNumbersByCity(CitiesList.Text))
            {
                NumbersList.Items.Add(city.CityPhoneCode+phone.Number);
            }
        }


        private void NumbersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void NumbersList_DoubleClick(object sender, EventArgs e)
        {
            
            NumberEdit numberEdit = new NumberEdit(NumbersList.Text, CitiesList.Text);
            numberEdit.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddNumberForm form = new AddNumberForm(CitiesList.Text);
            form.Show();
        }

        private void CitiesList_Click(object sender, EventArgs e)
        {
            NumbersList.Items.Clear();
            CityServices cityServices = new CityServices();
            var city = cityServices.GetCitiesList().Where(x => x.CityName == CitiesList.Text).FirstOrDefault();
            foreach (var phone in cityServices.GetNumbersByCity(CitiesList.Text))
            {
                NumbersList.Items.Add(city.CityPhoneCode + phone.Number);
            }
        }
    }
}
