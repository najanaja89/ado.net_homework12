﻿using ado.net_homework12.DataContext;
using ado.net_homework12.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ado.net_homework12
{
    public partial class NumberEdit : Form
    {
        public NumberEdit(string number, string city)
        {
            CityServices cityServices = new CityServices();
            number = number.Remove(0, 4);
            InitializeComponent();
            textBox1.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox1.Text = number;
            textBox3.Text = city;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CityServices cityServices = new CityServices();
            using (var context = new CityContext())
            {
                var singleCity = context.Cities.Where(x => x.CityName == textBox3.Text).ToList().FirstOrDefault();
                var phoneNumbersList = context.PhoneNumbers.Where(x => x.CitiesId == singleCity.Id).Where(x => x.Number == textBox1.Text).ToList().FirstOrDefault();
                if (phoneNumbersList == null)
                {
                    MessageBox.Show("Number not exists");
                }
                else
                {
                    if (Regex.IsMatch(textBox2.Text, @"^\d+$"))
                    {
                        phoneNumbersList.Number = textBox2.Text;
                        context.SaveChanges();
                    }
                    else
                    {
                        MessageBox.Show("field supports only digits");
                    }
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {


        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void NumberEdit_Load(object sender, EventArgs e)
        {

        }
    }
}
