namespace ado.net_homework12.DataContext
{
    using ado.net_homework12.Models;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class CityContext : DbContext
    {
        // Your context has been configured to use a 'CityContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ado.net_homework12.DataContext.CityContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'CityContext' 
        // connection string in the application configuration file.
        public CityContext()
            : base("name=CityContext")
        {
            Database.SetInitializer(new DataInitialization());
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<PhoneNumbers> PhoneNumbers { get; set; } 

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}