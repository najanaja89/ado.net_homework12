﻿using ado.net_homework12.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework12.DataContext
{
    public class DataInitialization : DropCreateDatabaseIfModelChanges<CityContext>
    {
        //public override void InitializeDatabase(CityContext context)
        //{
        //    context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction
        //        , string.Format("ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE", context.Database.Connection.Database));

        //    base.InitializeDatabase(context);
        //}


        protected override void Seed(CityContext context)
        {
            List<City> citiesList = new List<City>
            {
                 new City { CityName ="Wakanda",          CityPhoneCode="3326" },
                 new City { CityName ="Gorillas City",    CityPhoneCode="5698" },
                 new City { CityName ="New Junk City",    CityPhoneCode="3925" },
                 new City { CityName ="Central City",     CityPhoneCode="9687" },
                 new City { CityName ="Star City",        CityPhoneCode="2536" },
            };

            List<PhoneNumbers> PhoneNumbersList = new List<PhoneNumbers>
            {
                 new PhoneNumbers { CitiesId=citiesList[0].Id,  Number="368898"},
                 new PhoneNumbers { CitiesId=citiesList[0].Id,  Number="987855"},
                 new PhoneNumbers { CitiesId=citiesList[1].Id,  Number="956688"},
                 new PhoneNumbers { CitiesId=citiesList[3].Id,  Number="669822"},
                 new PhoneNumbers { CitiesId=citiesList[4].Id,  Number="113366"},
                 new PhoneNumbers { CitiesId=citiesList[1].Id,  Number="445588"},
                 new PhoneNumbers { CitiesId=citiesList[0].Id,  Number="578966"},
                 new PhoneNumbers { CitiesId=citiesList[2].Id,  Number="921129"},
                 new PhoneNumbers { CitiesId=citiesList[3].Id,  Number="890808"},
                 new PhoneNumbers { CitiesId=citiesList[3].Id,  Number="115566"},
                 new PhoneNumbers { CitiesId=citiesList[1].Id,  Number="195588"},
                 new PhoneNumbers { CitiesId=citiesList[0].Id,  Number="159123"},
                 new PhoneNumbers { CitiesId=citiesList[4].Id,  Number="941124"},
                 new PhoneNumbers { CitiesId=citiesList[3].Id,  Number="557897"},
            };
            context.Cities.AddRange(citiesList);
            context.PhoneNumbers.AddRange(PhoneNumbersList);
            context.SaveChanges();
        }
    }
}
