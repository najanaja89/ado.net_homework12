﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework12.Models
{
    public class PhoneNumbers:Entity
    {
        public string Number { get; set; }
        public Guid CitiesId { get; set; }
        public virtual City Cities { get; set; }
    }
}
